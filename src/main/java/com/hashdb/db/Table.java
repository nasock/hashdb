package com.hashdb.db;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.HashMap;

public class Table {
	private String fileName;
	private HashMap<DataObject, DataObject> table;

	public Table(String flName) throws TableReadException {
		this.fileName = flName;
		table = new HashMap<DataObject, DataObject>();
		read();
	}

	public void put(DataObject key, DataObject value) {
		table.put(key, value);
	}

	public DataObject get(DataObject key) {
		return table.get(key);
	}

	public void delete(DataObject key) {
		table.remove(key);
	}

	public void save() throws TableWriteException {
		try(DataOutputStream dos = new DataOutputStream(new BufferedOutputStream(new FileOutputStream(fileName)))) {
			// FileOutputStream fos = new FileOutputStream(fileName);
			// DataOutputStream dos = new DataOutputStream(fos);

			int size = table.size();
			try {
				dos.writeInt(size);
				for (DataObject key : table.keySet()) {
					DataObject value = table.get(key);
					writeObject(key, dos);
					writeObject(value, dos);
				}
			} finally {
				dos.flush();
				dos.close();
			}
		} catch (FileNotFoundException e) {
			throw new TableWriteException();
		} catch (IOException e) {
			e.printStackTrace();
			throw new TableWriteException();
		}
	}

	private void writeObject(DataObject object, DataOutputStream dos) throws IOException {
		short type = object.getType();
		byte[] bytes = object.getBytes();
		int size = bytes.length;
		dos.writeShort(type);
		dos.writeInt(size);
		dos.write(bytes);
	}

	private void read() throws TableReadException {
		table = new HashMap<DataObject, DataObject>();
		try {
			// FileInputStream fis = new FileInputStream(fileName);
			// DataInputStream dis = new DataInputStream(fis);

			DataInputStream dis = new DataInputStream(new BufferedInputStream(new FileInputStream(fileName)));

			try {
				int size = dis.readInt();
				for (int i = 0; i < size; i++) {
					DataObject key = readObject(dis);
					DataObject value = readObject(dis);
					table.put(key, value);
				}
			} finally {
				
				dis.close();
			}
		} catch (FileNotFoundException e) {

		} catch (IOException e) {
			e.printStackTrace();
			throw new TableReadException();
		}
	}

	private DataObject readObject(DataInputStream dis) throws IOException {
		short type = dis.readShort();
		int size = dis.readInt();
		byte[] bytes = new byte[size];
		dis.read(bytes);
		if (type == DataType.DOUBLE) {
			return new DODouble(bytes);
		} else {
			return new DOString(bytes);
		}
	}

	public String toString() {
		StringBuilder strb = new StringBuilder();
		strb.append("{");
		for (DataObject key : table.keySet()) {
			DataObject value = table.get(key);
			strb.append(key.toString() + ":" + value.toString() + ", ");
		}
		strb.append("}");
		return strb.toString();
	}

	public static void main(String[] args) {
		try {
			Table t = new Table("1.hdb");
			t.put(new DOString("str"), new DODouble(2.0));
			System.out.println(t.toString());
			t.save();
			t.read();
			System.out.println(t.toString());
		} catch (TableWriteException e) {
			e.printStackTrace();
		} catch (TableReadException e) {
			e.printStackTrace();
		}

		// try {
		// FileOutputStream fos = new FileOutputStream("1.txt");
		// fos.write(1);
		// fos.close();
		// } catch (FileNotFoundException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// } catch (IOException e) {
		// // TODO Auto-generated catch block
		// e.printStackTrace();
		// }
	}

}
