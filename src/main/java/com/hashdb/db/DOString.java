package com.hashdb.db;

public class DOString extends DataObject {
	private String value;

	public DOString(String str) {
		this.value = str;
	}
	
	public DOString(byte[] bytes) {
		this.value = ByteUtils.bytesToString(bytes);
	}

	public String getValue() {
		return value;
	}

	@Override
	public byte[] getBytes() {
		return ByteUtils.stringToBytes(value);
	}

	@Override
	public short getType() {
		return DataType.STRING;
	}

	@Override
	public String toString() {
		return value;
	}
}
