package com.hashdb.db;

import java.nio.ByteBuffer;

public class ByteUtils {

	public static byte[] stringToBytes(String value) {
		return value.getBytes();
	}

	public static byte[] doubleToBytes(double value) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES);
		byteBuffer.putDouble(value);
		return byteBuffer.array();
	}

	public static String bytesToString(byte[] bytes) {
		return new String(bytes);
	}

	public static double bytesToDouble(byte[] bytes) {
		ByteBuffer byteBuffer = ByteBuffer.allocate(Double.BYTES);
		byteBuffer.put(bytes);
		byteBuffer.flip();
		return byteBuffer.getDouble();
	}

	public static void main(String[] args) {
		String d = "str";
		byte[] bs = stringToBytes(d);
		String d1 = bytesToString(bs);
		System.out.println(bs + "\n" + d1);
	}
}
