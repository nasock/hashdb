package com.hashdb.db;

public class DODouble extends DataObject {
	private double value;

	public DODouble(double dbl) {
		this.value = dbl;
	}
	
	public DODouble(byte[] bytes) {
		this.value = ByteUtils.bytesToDouble(bytes);
	}

	public double getValue() {
		return value;
	}
	
	@Override
	public byte[] getBytes() {
		return ByteUtils.doubleToBytes(value);
	}

	@Override
	public short getType() {
		return DataType.DOUBLE;
	}

	@Override
	public String toString() {
		return ""+value;
	}

}
