package com.hashdb.db;

public abstract class DataObject {
	
	public abstract byte[] getBytes();
	
	public abstract short getType();
	
	public abstract String toString();
}

